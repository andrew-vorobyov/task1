import React, {Component} from 'react';
import axios from 'axios';

import {groupsUrl, groupEditUrl} from '../../../../config'

class FormEditGroup extends Component {

    constructor(props) {

        super(props)
        this.state = {
            group: '',
            _id: '',

            errors: {},
        };
        this.handleFormGroup = this.handleFormGroup.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleReturn = this.handleReturn.bind(this);
        this.resetFormData = this.resetFormData.bind(this);
        this.renderErrors = this.renderErrors.bind(this);
        this.getGroup = this.getGroup.bind(this);
    }

    componentDidMount() {
        this.getGroup()
    }

    resetFormData() {
        this.setState({
            group: '',
        })
    }

    handleFormSubmit(event) {
        event.preventDefault();

        const formData = {
            _id: this.props.match.params.id,
            group: this.state.group,
        }
        //axios
        axios.post(groupEditUrl, formData,
            {
                headers: {authorization: localStorage.getItem('token')}
            }
            )
            .then(res => {

                if (res.data.error) {
                    console.log('ERROR: ' + res.data.error);
                } else {
                    this.resetFormData();
                    this.setState({errors: {}});
                    this.props.history.push('/groups');
                }

            }).catch(error => {
            this.setState({errors: 'Probably you have problem with your network'});
            console.log(error);
        })
    }

    handleFormGroup(event) {
        this.setState({group: event.target.value});
    }

    handleReturn() {
        this.props.history.push('/groups');
    }

    getGroup() {
        const self = this;

        axios.get(groupsUrl + '/' + this.props.match.params.id,
            {
                headers: {authorization: localStorage.getItem('token')}
            }
        ).then(res => {
            if (res.data.error) {
                console.log('ERROR: ' + res.data.error);
            } else {
                self.setState({
                    _id: res.data.result[0]._id,
                    group: res.data.result[0].group,
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }

    renderErrors() {
        if (JSON.stringify(this.state.errors) !== '{}') {
            if (this.state.errors) {
                return (
                    <div>
                        <div style={{'color': 'red'}}>*{this.state.errors}</div><br/>
                    </div>
                );
            }
        }
    }

    render() {
        return (
            <div className="FormEditGroup">

                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <div className="panel">
                                <div className="page-header">
                                    <h1>Form Edit Group</h1>
                                </div>
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-lg-12">

                                            <form id="group-form" style={{display: 'block'}}
                                                  onSubmit={this.handleFormSubmit}>
                                                <div className="form-group">
                                                    <label htmlFor="group">Group</label>
                                                    <input required type="text" name="group"
                                                           id="group" tabIndex="7" className="form-control"
                                                           placeholder="Group"
                                                           value={this.state.group}
                                                           onChange={this.handleFormGroup}/>
                                                </div>
                                                {this.renderErrors()}
                                                <div className="form-group">
                                                    <div className="row">
                                                        <div className="col-sm-6 col-sm-offset-3">
                                                            <input type="submit" name="group-submit"
                                                                   id="group-submit" tabIndex="7"
                                                                   className="form-control btn btn-success"
                                                                   value="Save group"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button className="btn btn-default" onClick={this.handleReturn} tabIndex="8">Return</button>
            </div>
        );
    }
}

export default FormEditGroup;