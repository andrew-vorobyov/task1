import React, {Component} from 'react';
import axios from 'axios';
import {groupsAllGroupsUrl, groupsUrl} from '../../../../config';

import Pagination from "react-js-pagination";

class Groups extends Component {

    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            groupDelete: {},
            groupEdit: {},

            // variables for pagination component
            activePage:1,
            itemsCountPerPage: 10,
            totalItemsCount: 10,
            pageRangeDisplayed: 5,
        }
        this.getGroups = this.getGroups.bind(this);
        this.renderGroups = this.renderGroups.bind(this);
        this.handleDeleteGroup = this.handleDeleteGroup.bind(this);
        this.handleAddGroup = this.handleAddGroup.bind(this);
        this.handleEditGroup = this.handleEditGroup.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        this.getGroups()
    }

    handlePageChange(pageNumber) {
        this.getGroups(pageNumber);
        this.setState({activePage: pageNumber});
    }

    getGroups(pageNumber) {

        let pageNumberFinal = pageNumber || this.state.activePage;
        let url = groupsAllGroupsUrl + '?pageNumber=' + pageNumberFinal;

        axios.get(url)
            .then(res => {
                if (res.data.error) {
                    console.log('ERROR: ' + res.data.error);
                } else {
                    // this.setState({groups: res.data.result});

                    this.setState({groups: res.data.result.result});
                    this.setState({totalItemsCount: res.data.result.count});
                }
            }).catch(error => {
            console.log(error);
            this.setState({groups: []});
        });
    }

    handleAddGroup() {
        this.props.history.push('/groups/add');
    }


    handleEditGroup(event) {
        const id = event.target.getAttribute('data-id')
        this.props.history.push('/groups/edit/' + id);

    }

    handleDeleteGroup(event) {
        const id = event.target.getAttribute('data-id')

        var confirmDelete = confirm('Are you sure that you want to delete?');

        if (!confirmDelete) {
        } else {
            axios.delete(groupsUrl + '/' + id,
                {
                    headers: {authorization: localStorage.getItem('token')}
                }
            ).then(res => {
                if (res.data.error) {
                    console.log('ERROR: ' + res.data.error);
                } else {

                    this.getGroups()
                }
            }).catch(error => {
                console.log(error);
            })
        }
    }

    renderGroups() {

        if (this.state.groups.length !== 0) {

            let counter = (this.state.activePage - 1) * 10;
            return this.state.groups.map(group =>
                <tr key={group._id}>
                    <td>
                        <span>{++counter} </span><br/>
                    </td>
                    <td>
                        <span>{group.group} </span><br/>
                    </td>
                    <td>

                        <button className="btn btn btn-static-size" data-id={group._id}
                                onClick={this.handleEditGroup}>
                            <span className="text" data-id={group._id}>Edit</span>
                            <span className="icons" data-id={group._id}><i className="fa fa-pencil-square-o"
                                                                           aria-hidden="true"></i></span>
                        </button>

                        <button className="btn btn-danger btn-static-size" data-id={group._id}
                                onClick={this.handleDeleteGroup}>
                            <span className="text" data-id={group._id}>Delete</span>
                            <span className="icons" data-id={group._id}><i className="fa fa-times fa-lg"
                                                                           aria-hidden="true"></i></span>
                        </button>
                    </td>
                </tr>
            )
        } else {
            return (
                <tr>
                    <td>
                        <span>!!!</span><br/>
                    </td>
                    <td>
                        <span>There are no groups</span><br/>
                    </td>
                    <td>
                        <span></span><br/>
                    </td>
                </tr>
            )
        }
    }

    render() {
        return (
            <div className="Groups">
                <div>
                    <div className="page-header clearfix">
                        <h1 className="pull-left">Groups</h1>
                        <button onClick={this.handleAddGroup} className="btn btn-primary pull-right">Add group</button>
                    </div>

                    <table className="table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th className="group-title-width">Title</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.renderGroups()}
                        </tbody>
                    </table>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="text-center">
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.itemsCountPerPage}
                                totalItemsCount={this.state.totalItemsCount}
                                pageRangeDisplayed={this.state.pageRangeDisplayed}
                                onChange={this.handlePageChange}
                            />
                        </div>
                    </div>

                </div>

            </div>
        );
    }
}

export default Groups;