import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {unauthUser, authUser} from '../../../actions/index';

class LogoutPage extends Component {

    componentWillMount() {

        if (localStorage.getItem('token')) {

            this.props.unauthUser()
            this.props.authUser()
            this.props.unauthUser()

            localStorage.removeItem('token');
            // localStorage.removeItem('role');
            localStorage.removeItem('email');
            this.props.history.push('/login');
        } else {
            this.props.history.push('/tests');
        }
    }

    render() {
        return (
            <div className="LogoutPage">
                <h2>Sorry to see you go...</h2>
            </div>
        );
    }
}

export default LogoutPage = connect(
    (state) => {return {authStatus: state.authStatus}},
    (dispatch) => {return bindActionCreators({unauthUser, authUser}, dispatch)}
)(LogoutPage);
