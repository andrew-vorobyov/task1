import React, {Component} from 'react';
import axios from 'axios';
import {registrationUrl} from '../../../config'

class RegistrationPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            confirmPassword: '',

            errors: []
        };
        this.handleFormUsername = this.handleFormUsername.bind(this);
        this.handleFormEmail = this.handleFormEmail.bind(this);
        this.handleFormPassword = this.handleFormPassword.bind(this);
        this.handleFormConfirmPassword = this.handleFormConfirmPassword.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);

        this.renderErrors = this.renderErrors.bind(this);
    }

    handleFormUsername(event) {
        this.setState({username: event.target.value});
    }

    handleFormEmail(event) {
        this.setState({email: event.target.value});
    }

    handleFormPassword(event) {
        this.setState({password: event.target.value});
    }

    handleFormConfirmPassword(event) {
        this.setState({confirmPassword: event.target.value});
    }

    handleFormSubmit(event) {

        event.preventDefault();

        this.setState({errors: []});

        //validation (html5 and check password and confirmPassword to match)
        if (this.state.password !== this.state.confirmPassword) {
            const newErrors = []
            newErrors.push('Password and Confirm Password is not match')
            this.setState({errors: newErrors});
            return false
        }

        const self = this;

        //axios
        axios.post(registrationUrl, {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
        }, {
            headers: {'Authorization': localStorage.getItem('token')}
        }).then(res => {

            if (res.data.error) {
                const newErrors = [...self.state.errors]
                newErrors.push(res.data.error)
                self.setState({errors: newErrors});
            } else {
                const newErrors = [...self.state.errors]
                newErrors.push(res.data.result)
                self.setState({errors: newErrors});

                this.setState({username: ''})
                this.setState({email: ''})
                this.setState({password: ''})
                this.setState({confirmPassword: ''})
            }

        }).catch(error => {
            const newErrors = [...self.state.errors]
            newErrors.push('Error Network')
            self.setState({errors: newErrors});
        })
    }

    renderErrors() {
        if (this.state.errors.length !== 0) {
            return this.state.errors.map((error, i) => <li key={i}>{error}</li>);
        }
    }

    render() {

        return (
            <div className="RegistrationPage">

                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <div className="panel panel-login">
                                <div className="page-header">
                                    <h1>Registration</h1>
                                </div>
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <form id="register-form" style={{display: 'block'}}
                                                  onSubmit={this.handleFormSubmit}>
                                                <div className="form-group">
                                                    <label htmlFor="username">Username</label>
                                                    <input required type="text" name="username" id="username"
                                                           tabIndex="1" className="form-control" placeholder="Username"
                                                           value={this.state.username}
                                                           onChange={this.handleFormUsername}/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="email">Email</label>
                                                    <input required type="email" name="email" id="email" tabIndex="1"
                                                           className="form-control" placeholder="Email Address"
                                                           value={this.state.email}
                                                           onChange={this.handleFormEmail}/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="password">Password</label>
                                                    <input required type="password" name="password" id="password"
                                                           tabIndex="2" className="form-control" placeholder="Password"
                                                           value={this.state.password}
                                                           onChange={this.handleFormPassword}/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="confirm-password">Confirm Password</label>
                                                    <input required type="password" name="confirm-password"
                                                           id="confirm-password" tabIndex="2" className="form-control"
                                                           placeholder="Confirm Password"
                                                           value={this.state.confirmPassword}
                                                           onChange={this.handleFormConfirmPassword}/>
                                                </div>

                                                <div className="errors">
                                                    <ul>
                                                        {this.renderErrors()}
                                                    </ul>
                                                </div>

                                                <div className="form-group">
                                                    <div className="row">
                                                        <div className="col-sm-6 col-sm-offset-3">
                                                            <input type="submit" name="register-submit"
                                                                   id="register-submit" tabIndex="4"
                                                                   className="form-control btn btn-primary btn-register"
                                                                   value="Register Now"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default RegistrationPage;