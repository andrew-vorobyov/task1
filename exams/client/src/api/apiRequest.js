import {checkadmin} from '../config'
import axios from 'axios';

export function getCheckAdmin() {
    const request = axios.post(checkadmin, null, {headers: {authorization: localStorage.getItem('token')}})
    return request;
}