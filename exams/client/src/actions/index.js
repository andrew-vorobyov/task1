import {getCheckAdmin} from '../api/apiRequest';

export const AUTH_USER = 'AUTH_USER';
export const UNAUTH_USER = 'UNAUTH_USER';
export const CHECK_ADMIN = 'CHECK_ADMIN';

export function authUser() {
    return {
        type: AUTH_USER,
    }
}

export function unauthUser() {
    return {
        type: UNAUTH_USER,
    }
}

export function checkAdmin() {
    return {
        type: CHECK_ADMIN,
        payload: getCheckAdmin()
    }
}
