export const apiDomein = 'http://localhost:3001/';

// *** auth and registration  ***
export const authUrl = apiDomein + 'auth';
export const checkadmin = authUrl + '/checkadmin';
export const registrationUrl = apiDomein + 'registration';

// *** groups ***
export const groupsUrl = apiDomein + 'groups';
export const groupsAllUrl = groupsUrl + '/all';
export const groupsAllGroupsUrl = groupsUrl + '/allgroups';
export const groupAddUrl = groupsUrl + '/add';
export const groupEditUrl = groupsUrl + '/edit';

