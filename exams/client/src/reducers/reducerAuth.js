export default function (state = false, action) {
    switch (action.type) {
        case 'AUTH_USER':
            return true;
        case 'UNAUTH_USER':
            return false;
        default:
            return state;
    }
}