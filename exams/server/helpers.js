// JWT
var jwt = require('jwt-simple');

var config = require('./config');

/*
 HELPER FUNCTIONS
 */

var helpers = {

    getDataFromToken(token) {

        try {
            var tokenData = jwt.decode(token, config.secret);
        }
        catch (err) {
            console.log(err)
        }

        return tokenData
    },

    checkToken: function (token) {
        var checkToken = true;

        try {
            var tokenData = jwt.decode(token, config.secret);

            if (tokenData.expire < Date.now()) {
                checkToken = false;
            }
        }
        catch (err) {
            console.log(err);
            checkToken = false;
        }

        return checkToken
    },

    checkAdmin: function (email) {
        var result = config.arrAdmins.indexOf(email);

        if (result != -1) {
            return true;
        } else {
            return false;
        }
    }

};

module.exports = helpers;
