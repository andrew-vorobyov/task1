/**
 * Module dependencies.
 */

var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jwt-simple');

/**
 * Module configurations.
 */

var config = require('../config');

/**
 * Routes.
 */

router.get('/', function (req, res, next) {

    if (!req.headers.authorization) {
        res.send(JSON.stringify({'error': 'you request do not have token'}));
    } else {

        var token = req.headers.authorization;
        var checkToken = true;

        try {
            var tokenData = jwt.decode(token, config.secret);

            if (tokenData.expire < Date.now()) {
                checkToken = false;
            }
        }
        catch (err) {
            console.log(err);
            checkToken = false;
        }

        if (!checkToken) {
            res.send(JSON.stringify({'error': 'your token is not valid or have been expire'}))
        } else {
            // get data from db, check exist user, response
            MongoClient.connect('mongodb://localhost:27017/myappdb', function (err, db) {
                if (err) throw err;

                db.collection('users').find().toArray(function (err, result) {
                    if (err) throw err;
                    res.send(JSON.stringify({'result': result}))
                })
            })
        }
    }
});


module.exports = router;
