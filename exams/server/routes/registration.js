/**
 * Module dependencies.
 */

var express = require('express');
var router = express.Router();
var helpers = require('../helpers');
var MongoClient = require('mongodb').MongoClient;
var bcrypt = require('bcrypt-nodejs');

/**
 * Module configurations.
 */

var config = require('../config');

/**
 * Routes.
 */

router.post('/', function (req, res) {

    // get data from req
    var username = req.body.username;
    var email = req.body.email;

    // get data from db, check exist user, response
    MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
        if (err) throw err;

        db.collection('users').find({"username": username, "email": email}).toArray(function (err, result) {
            if (err) throw err;

            if (result.length != 0) {
                res.send(JSON.stringify({'error': 'user already axist'}));
            } else {

                var userData = (!helpers.checkAdmin(email)) ? {
                    username: username,
                    email: email,
                    password: bcrypt.hashSync(req.body.password),
                    role: 'subscriber'
                } : {
                    username: username,
                    email: email,
                    password: bcrypt.hashSync(req.body.password),
                    role: 'admin'
                };

                db.collection('users').insertOne(userData);
                res.send(JSON.stringify({'result': 'user was added success'}))
            }
        })
    })
});


module.exports = router;