/**
 * Module dependencies.
 */

var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var MongoClient = require('mongodb').MongoClient;
var bcrypt = require('bcrypt-nodejs');
var helpers = require('../helpers');
var config = require('../config');

/**
 * Routes.
 */

router.post('/', function (req, res) {

    // 1. Simple Auth
    if (req.body.username && req.body.password) {

        // get data from req
        var username = req.body.username;
        var password = req.body.password;

        // get data from db, check exist user, response
        MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
            if (err) throw err;

            db.collection('users').find({"username": username}).toArray(function (err, result) {
                if (err) throw err;

                if (result.length == 0) {
                    res.status(404);
                    res.send(JSON.stringify({'error': 'user not found'}));
                } else {

                    if (bcrypt.compareSync(password, result[0].password)) {

                        var userData = {
                            id: result[0]._id,
                            email: result[0].email,
                            username: result[0].username,
                            role: result[0].role,
                            expire: Date.now() + 86400
                        };

                        var token = jwt.encode(userData, config.secret);

                        res.send(JSON.stringify({'token': token, 'email': email}))
                    } else {
                        res.status(404);
                        res.send(JSON.stringify({'error': 'user not found'}));
                    }
                }
            })
        })
    }
});

router.post('/checkadmin', function (req, res) {

    if (!req.headers.authorization) {
        res.status(401);
        res.send(JSON.stringify({'error': 'you request do not have token'}));
    } else {

        var resultCheckToken = helpers.checkToken(req.headers.authorization);
        var userEmail = helpers.getDataFromToken(req.headers.authorization).email;
        var resultCheckAdmin = helpers.checkAdmin(userEmail);

        if (!resultCheckToken && !resultCheckAdmin) {
            res.status(401);
            res.send(JSON.stringify({'error': 'your token is not valid or have been expire or you do not have need permission'}))
        } else {
            res.send({'result': true});
        }
    }
});


module.exports = router;