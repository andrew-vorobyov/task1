
var config = {

    // ********** admins **********
    arrAdmins : [
        'andrey.vorobev@uadevelopers.com',
    ],

    secret : 'xxxxx',

    
    // ***** mongo *****
    // connectDatabaseUrl : 'mongodb://localhost:27017/exams',
    connectDatabaseUrl : 'mongodb://mongo:27017/exams'

};

module.exports = config;
